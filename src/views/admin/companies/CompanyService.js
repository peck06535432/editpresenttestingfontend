const companyService = {
  companyList: [
    {
      id: 1,
      name: 'ธนาคารไทยพาณิชย์ (SCB)',
      phone: '0277775555',
      email: 'scbprime@scb.co.th',
      address:
        'ธนาคารไทยพาณิชย์ จำกัด (มหาชน) สำนักงานใหญ่ 9 ถ.รัชดาภิเษก เขตจตุจักร กรุงเทพฯ 10900'
    },
    {
      id: 2,
      name: 'บริษัท ไมโครซอฟท์ (ประเทศไทย) จำกัด',
      phone: '0225749999',
      email: 'microsoft@thailand.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    },
    {
      id: 3,
      name: 'กูเกิล Google Thailand (@GoogleThailand)',
      phone: '022574999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    },
    {
      id: 4,
      name: 'Apple (Thailand)',
      phone: '0225749999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    },
    {
      id: 5,
      name: 'ซัมซุง ประเทศไทย (บ.ไทยซัมซุงอิเลคโทรนิคส์ จก.)',
      phone: '0225749999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    }
  ],
  lastId: 6,
  selectCompany: '',
  addCompany (company) {
    company.id = this.lastId++
    this.companyList.push(company)
  },
  updateCompany (company) {
    const index = this.companyList.findIndex(item => item.id === company.id)
    this.companyList.splice(index, 1, company)
  },
  deleteCompany (company) {
    const index = this.companyList.findIndex(item => item.id === company.id)
    this.companyList.splice(index, 1)
  },
  getCompanies () {
    return [...this.companyList]
  }
}

export default companyService
