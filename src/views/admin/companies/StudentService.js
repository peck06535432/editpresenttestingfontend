const studentService = {
  studentList: [
    {
      id: 1,
      studentId: '60160238',
      name: 'ฉัตรตราวุธ ภูละคร',
      role: 'Programer',
      company: 'ธนาคารไทยพาณิชย์ (SCB)'
    },
    {
      id: 2,
      studentId: '60160238',
      name: 'ฉัตรตราวุธ ภูละคร',
      role: 'Tester',
      company: 'ธนาคารไทยพาณิชย์ (SCB)'
    },
    {
      id: 3,
      studentId: '60160238',
      name: 'ฉัตรตราวุธ ภูละคร',
      role: 'Tester',
      company: 'บริษัท ไมโครซอฟท์ (ประเทศไทย) จำกัด'
    }
  ],
  lastId: 4,
  addStudent (student) {
    student.id = this.lastId++
    this.studentList.push(student)
  },
  updateStudent (student) {
    const index = this.studentList.findIndex(item => item.id === student.id)
    this.studentList.splice(index, 1, student)
  },
  deleteStudent (student) {
    const index = this.studentList.findIndex(item => item.id === student.id)
    this.studentList.splice(index, 1)
  },
  getStudent () {
    return [...this.studentList]
  }
}

export default studentService
